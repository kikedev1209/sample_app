require 'test_helper'

class FollowingTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:michael)
    @other = users(:archer)
    log_in_as(@user)
  end
  
  test "following page" do
    get following_user_path(@user)
    assert @user.following.any?
    assert_match @user.following.count.to_s, response.body
    @user.following.each do |u|
      assert_select 'a[href=?]', user_path(u), text: u.name
    end
  end
  
  test "followers page" do
    get followers_user_path(@user)
    assert @user.followers.any?
    assert_match @user.followers.count.to_s, response.body
    @user.followers.each do |u|
      assert_select 'a[href=?]', user_path(u), text: u.name
    end
  end
  
  test "follow button the easy way" do
    assert_not @user.following.include?(@other.id)  # check first that :michael does not follow :archer
    assert_difference '@user.following.count', 1 do
      post relationships_path, params: { followed_id: @other.id }
    end
  end
  
  test "follow button with AJAX" do
    assert_not @user.following.include?(@other.id)  # check first that :michael does not follow :archer
    assert_difference '@user.following.count', 1 do
      post relationships_path, params: { followed_id: @other.id }, xhr: true
    end
  end
  
  test "unfollow button the easy way" do
    @user.follow(@other)
    relationship = @user.active_relationships.find_by(followed_id: @other.id)
    assert_difference '@user.following.count', -1 do
      delete relationship_path(relationship)
    end
  end
  
  test "unfollow button with AJAX" do
    @user.follow(@other)
    relationship = @user.active_relationships.find_by(followed_id: @other.id)
    assert_difference '@user.following.count', -1 do
      delete relationship_path(relationship), xhr: true
    end
  end

  test "feed on Home page" do
    get root_path
    @user.feed.paginate(page: 1).each do |micropost|
      assert_match CGI.escapeHTML(micropost.content), response.body
    end
  end
  
end
