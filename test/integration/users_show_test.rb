require 'test_helper'

class UsersShowTest < ActionDispatch::IntegrationTest

  def setup
    @user_m = users(:michael)   # active
    @user_l = users(:lana)      # inactive
  end

  test "should redirect to root_url when user not activated" do
    assert_not @user_l.activated
    get user_path(@user_l)
    assert_redirected_to root_url
  end

  test "should display user when activated" do
    assert @user_m.activated
    get user_path(@user_m)
    assert_template 'users/show'
    assert_response :success
  end
end