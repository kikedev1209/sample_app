require 'test_helper'

class MicropostsInterfaceTest < ActionDispatch::IntegrationTest
  # Integration test to log in, check the micropost pagination, make an invalid 
  # submission, make a valid submission, delete a post, and then visit a second 
  # user’s page to make sure there are no “delete” links.
  
  def setup
    @user = users(:michael)
  end
  
  test "micropost interface" do
    log_in_as(@user)
    get root_path
    assert_select 'div.pagination'
    assert_select 'input#micropost_picture'
    assert_select 'a', text: 'delete'
    # invalid post submission 
    assert_no_difference'Micropost.count' do
      post microposts_path, params: { micropost: { content: "" } } # invalid post
    end
    assert_select 'div#error_explanation'
    # valid post submission
    content = "This is a test post."
    picture = fixture_file_upload("test/fixtures/rails.png", 'image/png')
    assert_difference'Micropost.count', 1 do
      post microposts_path, params: { micropost: { content: content, picture: picture } }
    end
    assert assigns(:micropost).picture?
    assert_redirected_to root_url
    follow_redirect!
    assert_match content, response.body
    # delete a post
    assert_difference'Micropost.count', -1 do
      delete micropost_path(microposts(:orange))
    end
    # visit other user's page and check there are not 'delete' links
    get user_path(users(:archer))
    assert_template 'users/show'
    assert_select 'a', text: 'delete', count: 0
  end
  
  test "micropost sidebar count" do
    log_in_as(@user)
    get root_path
    assert_match "#{@user.microposts.count.to_s} microposts", response.body
    # User with zero microposts
    other_user = users(:malory)
    log_in_as(other_user)
    get root_path
    assert_match "0 microposts", response.body
    other_user.microposts.create!(content: "A micropost")
    get root_path
    assert_match "1 micropost", response.body
  end
  
end
