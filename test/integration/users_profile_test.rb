require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper
  
  def setup
    @user = users(:michael)
  end
  
  test "profile display" do
    get user_path(@user)
    assert_template 'users/show'
    assert_select 'title', full_title(@user.name)
    assert_select 'h1', @user.name
    assert_select 'h1>img.gravatar'
    # test stats
    assert_select 'div.stats', count: 1
    assert_select '#following'
    assert_match @user.following.count.to_s, response.body
    assert_select '#followers'
    assert_match @user.followers.count.to_s, response.body
    # test microposts
    assert_select 'h3', "Microposts (#{@user.microposts.count.to_s})"   # my version of testing microposts.count
    #assert_match @user.microposts.count.to_s, response.body             # same as previous line (tutotial version) -- less effective as it has to scan response.body
    assert_select 'div.pagination', count: 1
    @user.microposts.paginate(page: 1).each do |m|
      assert_select 'span.content', m.content       # my version of testing microposts.content
      #assert_match m.content, response.body         # same as previous line (tutotial version) -- same issue as before
    end
  end
end
