require 'test_helper'

class MicropostTest < ActiveSupport::TestCase

  def setup
    @user = users(:michael)
    @micropost = @user.microposts.new(content: "Lorem ipsum")    
  end
  
  test "micropost should be valid" do
    assert @micropost.valid?
  end
  
  test "micropost should not be valid" do
    @micropost.user_id = nil
    assert_not @micropost.valid?
  end
  
  test "micropost content should be present" do
    @micropost.content = "   "
    assert_not @micropost.valid?
  end
  
  test "micropost content should be max. 140 character long" do
    @micropost.content = "a" * 141
    assert_not @micropost.valid?
  end
  
  test "order should be most recent first" do
    assert_equal microposts(:most_recent), Micropost.first
  end
end
