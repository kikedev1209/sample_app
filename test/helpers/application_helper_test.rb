require 'test_helper'

class ApplicationHeperTest < ActionView::TestCase
  test "full_title helper" do
    assert_equal full_title, "Ruby on Rails Tutorial Sample App", "Wrong root title"
    assert_equal full_title("Help"), "Help | Ruby on Rails Tutorial Sample App", "Wrong Help title"
  end
end